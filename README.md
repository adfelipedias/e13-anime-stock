# Uma aplicação para o armazenamento de animes

## Tecnologias:
- Python
- Python Dotenv
- Flask
- PostgreSQL
- Blueprints
- Psycopg2

## Rotas:
GET, POST "/animes" - Se for **POST** cria a tabela no banco de dados, caso ela não exista. Se for **GET** mostra todos os dados da tabela.

GET "/animes/<anime_id>" - retorna um dado da tabela filtrado pelo id.

PATCH "/animes/<anime_id>" - atualiza os dados da requisição com os dados recebidos.

DELETE "/animes/<anime_id>" - deleta um dado do banco.
