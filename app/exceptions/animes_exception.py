class ColumnError(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)

class EmptyFile(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
    

class AnimeNotExists(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
