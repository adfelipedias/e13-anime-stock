from app.exceptions.animes_exception import AnimeNotExists, ColumnError, EmptyFile
from app.services.animes_service import Anime
from flask import Blueprint, request, jsonify
from psycopg2.errors import UniqueViolation

bp_animes = Blueprint("animes", __name__)


@bp_animes.route("/animes", methods=["POST", "GET"])
def get_create():
    if request.method == "POST":
        try:
            data = request.get_json()

            if "anime" in data:
                my_anime = {**data, "anime": str(data["anime"]).title()}
                anime = Anime(my_anime)
            else:
                anime = Anime(data)

            new_anime = anime.create()
            return new_anime, 201 
        except ColumnError as err:
            return err.args
        except UniqueViolation:
            return {"error": "anime is already exists"}, 409
 
    else:
        try:
            animes_list = Anime.get_all_animes()
            return {"data": animes_list}, 200
        except EmptyFile:
            return {"data": []}
    

@bp_animes.get("/animes/<int:anime_id>")
def filter(anime_id: int):
    try:
        anime = Anime.get_anime_by_id(anime_id)
        return anime
    except AnimeNotExists as err:
        return {"error": str(err)}, 404


@bp_animes.patch("/animes/<int:anime_id>")
def update(anime_id: int):
    try:
        data = request.get_json()
        anime = Anime.update_anime(anime_id, data)
        return anime, 200
    except ColumnError as err:
        return err.args
    except AnimeNotExists as err:
        return {"error": str(err)}


@bp_animes.delete("/animes/<int:anime_id>")
def delete(anime_id: int):
    try:
        anime = Anime.delete_anime(anime_id)
        return anime, 204
    except AnimeNotExists as err:
        return {"error": str(err)}