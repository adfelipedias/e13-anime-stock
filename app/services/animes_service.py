from app.exceptions.animes_exception import AnimeNotExists, ColumnError, EmptyFile
import psycopg2
from.configs import configs
from psycopg2 import sql
from typing import Union


def create_table():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    cur.execute(
        """
            CREATE TABLE IF NOT EXISTS animes (
                id                  bigserial           PRIMARY KEY,
                anime               varchar(100)        NOT NULL UNIQUE,
                released_date       date                NOT NULL,
                seasons             integer             NOT NULL
            );
        """
    )

    conn.commit()
    cur.close()
    conn.close()


def verify_column(data):
    values_instance = ["anime", "released_date", "seasons"]

    if type(data) == dict:
        values_column = [key for key in data.keys()]
    else:
        values_column = [key for key in data.__dict__.keys()]
    
    for column in values_column:
        if column not in values_instance:
            raise ColumnError({
                "available_keys": values_instance, 
                "worng_keys_sended": [column]
            }, 422)


class Anime():
    def __init__(self, data: Union[tuple, dict]):
        if type(data) is tuple:
            self.id, self.anime, self.released_date, self.seasons = data
        
        elif type(data) is dict:
            for key, value in data.items():
                setattr(self, key, value)

    
    def create(self):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()
        
        verify_column(self)

        columns = [sql.Identifier(key) for key in self.__dict__.keys()]
        values = [sql.Literal(value) for value in self.__dict__.values()]

        query = sql.SQL(
            """
                INSERT INTO
                    animes ({columns})
                VALUES
                    ({values})
                RETURNING *
            """).format(columns=sql.SQL(',').join(columns),
                        values=sql.SQL(',').join(values))

        cur.execute(query)
        fetch_result = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        serialized_anime = Anime(fetch_result).__dict__
        return serialized_anime


    @staticmethod
    def get_all_animes():
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute("SELECT * FROM animes;")

        fetch_result = cur.fetchall()

        if not fetch_result:
            raise EmptyFile

        conn.commit()
        cur.close()
        conn.close()

        serialized_anime = [Anime(animes).__dict__ for animes in fetch_result]
        return serialized_anime

    
    @staticmethod
    def get_anime_by_id(anime_id: int):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute(
            """
                SELECT * FROM animes
                WHERE id=%s
            """, (anime_id, )
        )

        fetch_anime = cur.fetchone()

        if not fetch_anime:
            raise AnimeNotExists("Not Found")
        
        conn.commit()
        cur.close()
        conn.close()
        
        anime = Anime(fetch_anime).__dict__
        return anime


    @staticmethod
    def update_anime(anime_id: int, data):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        verify_column(data)

        my_data = {**data, "anime": str(data["anime"]).title()} 
        columns = [sql.Identifier(key) for key in my_data.keys()]
        values = [sql.Literal(value) for value in my_data.values()]

        query = sql.SQL(
            """
                UPDATE 
                    animes
                SET
                    ({columns}) = row({values})
                WHERE
                    id={id}
                RETURNING *;
            """).format(id=sql.Literal(str(anime_id)),
                        columns=sql.SQL(',').join(columns),
                        values=sql.SQL(',').join(values))

        cur.execute(query)
        fetch_result = cur.fetchone()
        
        if not fetch_result:
            raise AnimeNotExists("Not Found")

        conn.commit()
        cur.close()
        conn.close()

        anime_update = Anime(fetch_result).__dict__
        return anime_update


    @staticmethod
    def delete_anime(anime_id: int):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute(
            """
                DELETE FROM
                    animes
                WHERE
                    id=%s
                RETURNING *;
            """, (anime_id, )
        )

        fetch_anime_deleted = cur.fetchone()

        if not fetch_anime_deleted:
            raise AnimeNotExists("Not Found")

        conn.commit()
        cur.close()
        conn.close()

        anime_deleted = Anime(fetch_anime_deleted).__dict__
        return anime_deleted

